using System;
using Xunit;

using Demo.Boaconic.Types.CSharp.Example;
using Newtonsoft.Json;
using Xunit.Abstractions;

namespace Demo.Boaconic.Types.CSharp.Tests
{
    public class GeneratedTypesShould
    {
        private ITestOutputHelper _outputHelper;
        
        public GeneratedTypesShould(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
        }
        
        [Fact]
        public void BeSerialisable()
        {
            var person = new Person
            {
                Name = "John Smith",
                Pets = new IPet[] {new Dog {Breed = "Spaniel", Name = "Fido"}},
                Status = Status.Married
            };

            var json = JsonConvert.SerializeObject(person);
            
            Assert.False(String.IsNullOrEmpty(json));
            
            _outputHelper.WriteLine(json);

            var personRt = JsonConvert.DeserializeObject<Person>(json);
            var jsonRt = JsonConvert.SerializeObject(personRt);
            
            Assert.Equal( json, jsonRt );

        }
    }
}
